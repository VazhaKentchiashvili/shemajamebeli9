package com.vazhasapp.shemajamebeli9

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vazhasapp.shemajamebeli9.databinding.ButtonItemCardViewBinding
import com.vazhasapp.shemajamebeli9.databinding.IconedButtonBinding

typealias ButtonClicker = (position: Int) -> Unit

class ButtonRecycler : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val ICONED_BUTTON = 0
        const val NUMBER_BUTTON = 1
    }

    private val buttonList = mutableListOf<Button>()
    lateinit var buttonClicker: ButtonClicker


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        if (viewType == 0) {
            IconedButtonViewHolder(
                IconedButtonBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                ))
        } else {
            ButtonViewHolder(
                ButtonItemCardViewBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder) {
            is ButtonViewHolder-> holder.bind()
            is IconedButtonViewHolder-> holder.bind()
        }
    }

    override fun getItemCount() = buttonList.size

    inner class ButtonViewHolder(private val binding: ButtonItemCardViewBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        private lateinit var currentButton: Button
        fun bind() {
            currentButton = buttonList[adapterPosition]
            binding.button.text = currentButton.number.toString()
        }

        override fun onClick(v: View?) {
            buttonClicker(adapterPosition)
        }
    }

    inner class IconedButtonViewHolder(private val binding: IconedButtonBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        private lateinit var currentButton: Button
        fun bind() {
            currentButton = buttonList[adapterPosition]
            binding.iconedButton.setImageResource(currentButton.icon!!)
        }

        override fun onClick(v: View?) {
            buttonClicker(adapterPosition)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (buttonList[position].number) {
            9 -> ICONED_BUTTON
            11 -> ICONED_BUTTON
            else -> NUMBER_BUTTON
        }
    }

    fun setData(button: MutableList<Button>) {
        buttonList.clear()
        buttonList.addAll(button)
        notifyDataSetChanged()
    }
}