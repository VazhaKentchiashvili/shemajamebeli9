package com.vazhasapp.shemajamebeli9

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.vazhasapp.shemajamebeli9.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val recyclerAdapter = ButtonRecycler()
    private val dummyButtonsData = mutableListOf<Button>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
    }

    private fun init() {
        setupRecycler()
        enterPinCode()
    }

    private fun setupRecycler() {
        val manager = GridLayoutManager(this, 3)
        manager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (dummyButtonsData.size % 2 != 0) {
                    (position == dummyButtonsData.size - 1)
                    2
                } else {
                    1
                }
            }
        }

        binding.recyclerView.apply {
            adapter = recyclerAdapter
            layoutManager = manager
            setHasFixedSize(true)
        }
        recyclerAdapter.setData(fillNumberData())
    }

    private fun enterPinCode() {
        recyclerAdapter.buttonClicker = {
            binding.btn1.setBackgroundColor(Color.GREEN)
        }
    }

    private fun fillNumberData(): MutableList<Button> {
        dummyButtonsData.add(Button(0))
        dummyButtonsData.add(Button(1))
        dummyButtonsData.add(Button(2))
        dummyButtonsData.add(Button(3))
        dummyButtonsData.add(Button(4))
        dummyButtonsData.add(Button(5))
        dummyButtonsData.add(Button(6))
        dummyButtonsData.add(Button(7))
        dummyButtonsData.add(Button(8))
        dummyButtonsData.add(Button(R.drawable.ic_fingerprint))
        dummyButtonsData.add(Button(0))
        dummyButtonsData.add(Button(R.drawable.ic_backspace))
        return dummyButtonsData
    }
}