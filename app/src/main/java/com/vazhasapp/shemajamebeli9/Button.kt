package com.vazhasapp.shemajamebeli9

data class Button(
    val number: Int? = null,
    val icon: Int? = null
)
